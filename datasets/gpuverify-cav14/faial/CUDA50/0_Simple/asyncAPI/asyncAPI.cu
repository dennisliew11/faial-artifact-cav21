//pass
//--blockDim=[512,1,1] --gridDim=[32768,1,1]

#include <cuda.h>






__global__ void kernel (int* g_data, int inc_value) {



    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    g_data[idx] = g_data[idx] + inc_value;

}
