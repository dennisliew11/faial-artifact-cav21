//pass
//--blockDim=[32] --gridDim=[1]

#include <cuda.h>

typedef unsigned int clock_t;
#define warpSize 32





__global__ void kernel (clock_t* d_clocks, int N) {



    __shared__ clock_t s_clocks[32];

    clock_t my_sum = 0;

    for (int i = threadIdx.x ; i < N ; i += blockDim.x)
    {
        my_sum += d_clocks[i];
    }

    s_clocks[threadIdx.x] = my_sum;
    __syncthreads();

    for (int i = warpSize / 2 ; i > 0 ; i /= 2)
    {
        if (threadIdx.x < i)
        {
            s_clocks[threadIdx.x] += s_clocks[threadIdx.x + i];
        }

        __syncthreads();
    }

    if (threadIdx.x == 0)
    {
        d_clocks[0] = s_clocks[0];
    }

}
