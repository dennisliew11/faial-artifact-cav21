//pass
//--blockDim=[128] --gridDim=[32768]

#include <cuda.h>
#include "common_merge.h"

#define sortDir 1





__global__ void kernel (int* d_DstKey, int* d_DstVal, int* d_SrcKey, int* d_SrcVal, int* d_LimitsA, int* d_LimitsB, int stride,int N) {

/* kernel pre-conditions */

__requires((stride & (stride - 1)) == 0);

__requires(stride < N);




    __shared__ uint s_key[2 * SAMPLE_STRIDE];
    __shared__ uint s_val[2 * SAMPLE_STRIDE];

    const int   intervalI = blockIdx.x & ((2 * stride) / SAMPLE_STRIDE - 1);
    const int segmentBase = (blockIdx.x - intervalI) * SAMPLE_STRIDE;
    d_SrcKey += segmentBase;
    d_SrcVal += segmentBase;
    d_DstKey += segmentBase;
    d_DstVal += segmentBase;

    //Set up threadblock-wide parameters
    __shared__ int startSrcA, startSrcB, lenSrcA, lenSrcB, startDstA, startDstB;

    if (threadIdx.x == 0)
    {
        int segmentElementsA = stride;
        int segmentElementsB = umin(stride, N - segmentBase - stride);
        int  segmentSamplesA = getSampleCount(segmentElementsA);
        int  segmentSamplesB = getSampleCount(segmentElementsB);
        int   segmentSamples = segmentSamplesA + segmentSamplesB;

        startSrcA    = d_LimitsA[blockIdx.x];
        startSrcB    = d_LimitsB[blockIdx.x];
        int endSrcA = (intervalI + 1 < segmentSamples) ? d_LimitsA[blockIdx.x + 1] : segmentElementsA;
        int endSrcB = (intervalI + 1 < segmentSamples) ? d_LimitsB[blockIdx.x + 1] : segmentElementsB;
        lenSrcA      = endSrcA - startSrcA;
        lenSrcB      = endSrcB - startSrcB;
        startDstA    = startSrcA + startSrcB;
        startDstB    = startDstA + lenSrcA;
    }

    //Load main input data
    __syncthreads();

    if (threadIdx.x < lenSrcA)
    {
        s_key[threadIdx.x +             0] = d_SrcKey[0 + startSrcA + threadIdx.x];
        s_val[threadIdx.x +             0] = d_SrcVal[0 + startSrcA + threadIdx.x];
    }

    if (threadIdx.x < lenSrcB)
    {
        s_key[threadIdx.x + SAMPLE_STRIDE] = d_SrcKey[stride + startSrcB + threadIdx.x];
        s_val[threadIdx.x + SAMPLE_STRIDE] = d_SrcVal[stride + startSrcB + threadIdx.x];
    }

    //Merge data in shared memory
    __syncthreads();
   int keyA, valA, keyB, valB, dstPosA, dstPosB;

    if (threadIdx.x < lenSrcA)
    {
        keyA = s_key[threadIdx.x];
        valA = s_val[threadIdx.x];
        dstPosA = binarySearchExclusive<sortDir>(keyA, s_key + SAMPLE_STRIDE, lenSrcB, SAMPLE_STRIDE) + threadIdx.x;
    }

    if (threadIdx.x < lenSrcB)
    {
        keyB = s_key[SAMPLE_STRIDE + threadIdx.x];
        valB =  s_val[SAMPLE_STRIDE + threadIdx.x];
        dstPosB = binarySearchInclusive<sortDir>(keyB, s_key, lenSrcA, SAMPLE_STRIDE) + threadIdx.x;
    }

    __syncthreads();

    if (threadIdx.x < lenSrcA)
    {
        s_key[dstPosA] = keyA;
        s_val[dstPosA] = valA;
    }

    if (threadIdx.x < lenSrcB)
    {
        s_key[dstPosB] = keyB;
        s_val[dstPosB] = valB;
    }

    //Store merged data
    __syncthreads();

    if (threadIdx.x < lenSrcA)
    {
        d_DstKey[startDstA + threadIdx.x] = s_key[threadIdx.x];
        d_DstVal[startDstA + threadIdx.x] = s_val[threadIdx.x];
    }

    if (threadIdx.x < lenSrcB)
    {
        d_DstKey[startDstB + threadIdx.x] = s_key[lenSrcA + threadIdx.x];
        d_DstVal[startDstB + threadIdx.x] = s_val[lenSrcA + threadIdx.x];
    }

}
