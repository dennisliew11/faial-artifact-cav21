//pass
//--blockDim=[32] --gridDim=[2]

#include <cuda.h>
#include "../common.h"






__global__ void kernel (float* g_resQ, float* g_rhsQ, float* g_Q, int Ntotal,float fa,float fb,float fdt) {



  
  int n = blockIdx.x * blockDim.x + threadIdx.x;
    
  if(n<Ntotal){
    float rhs = g_rhsQ[n];
    float res = g_resQ[n];
    res = fa*res + fdt*rhs;
    
    g_resQ[n] = res;
    g_Q[n]    += fb*res;
  }


}
