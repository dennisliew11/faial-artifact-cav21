//pass
//--blockDim=[64] --gridDim=[8]

#include <cuda.h>
#include "common.h"






__global__ void kernel (uint* id, uint* od, int w,int h,float a0,float a1,float a2,float a3,float b1,float b2,float coefp,float coefn) {

/* kernel pre-conditions */

__requires(w == 512);




    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;

    __requires (x < w);

    // forward pass
    float4 xp = make_float4(0.0f);  // previous input
    float4 yp = make_float4(0.0f);  // previous output
    float4 yb = make_float4(0.0f);  // previous output by 2
#if CLAMP_TO_EDGE
    xp = rgbaIntToFloat(*id);
    yb = coefp*xp;
    yp = yb;
#endif

    for (int y = 0; y < h; y++)
    {
        float4 xc = rgbaIntToFloat(id[x + y * w]);
        float4 yc = a0*xc + a1*xp - b1*yp - b2*yb;
        od[x + y * w] = rgbaFloatToInt(yc);
        xp = xc;
        yb = yp;
        yp = yc;
    }

    // reverse pass
    // ensures response is symmetrical
    float4 xn = make_float4(0.0f);
    float4 xa = make_float4(0.0f);
    float4 yn = make_float4(0.0f);
    float4 ya = make_float4(0.0f);
#if CLAMP_TO_EDGE
    xn = xa = rgbaIntToFloat(id[x]);
    yn = coefn*xn;
    ya = yn;
#endif

    for (int y = h-1;
         y >= 0; y--)
    {
        float4 xc = rgbaIntToFloat(id[x + y * w]);
        float4 yc = a2*xn + a3*xa - b1*yn - b2*ya;
        xa = xn;
        xn = xc;
        ya = yn;
        yn = yc;
        od[x + y * w] = rgbaFloatToInt(rgbaIntToFloat(od[x + y * w]) + yc);
    }

}
