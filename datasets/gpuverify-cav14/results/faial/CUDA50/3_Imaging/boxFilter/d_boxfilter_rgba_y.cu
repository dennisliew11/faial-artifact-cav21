//pass
//--blockDim=[64] --gridDim=[16]

#include <cuda.h>
#include "common.h"






__global__ void kernel (unsigned int* id, unsigned int* od, int w,int h,int r) {

/* kernel pre-conditions */

__requires(w == 1024);

__requires(h == 1024);





    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    float scale = 1.0f / (float)((r << 1) + 1);

    float4 t;
    // do left edge
    t = rgbaIntToFloat(id[x]) * r;

    for (int y = 0; y < (r + 1); y++)
    {
        t += rgbaIntToFloat(id[x+y*w]);
    }

    od[x] = rgbaFloatToInt(t * scale);

    for (int y = 1; y < (r + 1); y++)
    {
        t += rgbaIntToFloat(id[x + (y + r) * w]);
        t -= rgbaIntToFloat(id[x]);
        od[x + y * w] = rgbaFloatToInt(t * scale);
    }

    // main loop
    for (int y = (r + 1); y < (h - r); y++)
    {
        t += rgbaIntToFloat(id[x+ (y + r) * w]);
        t -= rgbaIntToFloat(id[x + ((y - r) * w) - w]);
        od[x + y * w] = rgbaFloatToInt(t * scale);
    }

    // do right edge
    for (int y = h - r; y < h; y++)
    {
        t += rgbaIntToFloat(id[x + (h - 1) * w]);
        t -= rgbaIntToFloat(id[x+ ((y - r) * w) - w]);
        od[x + y * w] = rgbaFloatToInt(t * scale);
    }

}
