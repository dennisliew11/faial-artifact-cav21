//pass
//--blockDim=[64] --gridDim=[16]

#include <cuda.h>
#include "common.h"






__global__ void kernel (float* id, float* od, int w,int h,int r) {

/* kernel pre-conditions */

__requires(w == 1024);

__requires(h == 1024);

__requires(r ==   14);





    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    d_boxfilter_y(&id[x], &od[x], w, h, r);

}
