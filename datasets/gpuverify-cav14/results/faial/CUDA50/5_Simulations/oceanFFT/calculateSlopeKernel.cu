//pass
//--blockDim=[8,8,1] --gridDim=[32,32,1]

#include <cuda.h>

#define width 256
#define height 256





__global__ void kernel (float* h, float2* slopeOut) {



unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
unsigned int i = y*width+x;

float2 slope = make_float2(0.0f, 0.0f);

if ((x > 0) && (y > 0) && (x < width-1) && (y < height-1))
{
    slope.x = h[i+1] - h[i-1];
    slope.y = h[i+width] - h[i-width];
}

slopeOut[i] = slope;

}
