//pass
//--blockDim=[256] --gridDim=[64]

#include <cuda.h>

#define TData int





__global__ void kernel (TData* d_odata, TData* d_idata, int numElements) {



    const int        tid = blockDim.x * blockIdx.x + threadIdx.x;
    const int numThreads = blockDim.x * gridDim.x;

    for (int pos = tid; pos < numElements; pos += numThreads)
    {
        d_odata[pos] = d_idata[pos];
    }

}
