//pass
//--blockDim=[256,1,1] --gridDim=[11377,1,1]

#include <cuda.h>
#include "common.h"






__global__ void kernel (const uint* startpoints, const uint* verticesMapping, uint* edges, uint edgesCount) {



    uint tid = blockIdx.x * blockDim.x + threadIdx.x;

    if (tid < edgesCount)
    {
        uint startpoint = startpoints[tid];
        uint &endpoint = edges[tid];

        uint newStartpoint = verticesMapping[startpoint];
        uint newEndpoint = verticesMapping[endpoint];

        if (newStartpoint == newEndpoint)
        {
            endpoint = UINT_MAX;
        }
    }

}
