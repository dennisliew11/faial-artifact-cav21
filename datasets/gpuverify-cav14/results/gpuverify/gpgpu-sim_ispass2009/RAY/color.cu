//pass
//--blockDim=[32,32] --gridDim=[32,32]

#include <cuda.h>
#include "common.h"






__global__ void kernel (uint* result, uint* Obj, float* prof, float3* A, float3* u, uint imageW,uint imageH,float pas) {



	uint x = __umul24(blockIdx.x, blockDim.x) + threadIdx.x;
    uint y = __umul24(blockIdx.y, blockDim.y) + threadIdx.y;
	uint id = x + y * imageW;
    __requires(x < imageW);
// Uncomment for a possible value of imageW
//   __requires(imageW == gridDim.x * blockDim.x + blockDim.x);
    float t(prof[id]);
    if( t > 0.0f  && t < 1000.0f ) {
        Rayon R;
        R.A = A[id];
        R.u = u[id];
        int idx = Obj[id];
        Sphere s(cnode[idx].s);
        s.C.x += pas;
        float4 f = make_float4(s.R,s.V,s.B,s.A)*(dot(getNormale(R.A+R.u*t,s.C),(-1.0f)*R.u));
        result[id] = rgbaFloatToInt(f);
    }
    else {
        result[id] = 0;
    }
    prof[id] = 100000.0f;

}
