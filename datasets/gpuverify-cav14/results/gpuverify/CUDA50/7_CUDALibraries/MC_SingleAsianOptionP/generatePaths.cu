//pass
//--blockDim=[128] --gridDim=[195]

#include <cuda.h>
#include "common.h"

#define Real float

__device__ static __attribute__((always_inline)) float getPathStep(float &drift, float &diffusion, curandState &state)
{
    return expf(drift + diffusion * curand_normal(&state));
}
__device__ static __attribute__((always_inline)) double getPathStep(double &drift, double &diffusion, curandState &state)
{
    return exp(drift + diffusion * curand_normal_double(&state));
}





__global__ void kernel (Real* paths, curandState* rngStates, AsianOption<Real>* option, int numSims,int numTimesteps) {

/* kernel pre-conditions */

__requires(numSims == 100000);

__requires(numTimesteps == 87);




    // Determine thread ID
    unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned int step = gridDim.x * blockDim.x;

    // Compute parameters
    Real drift     = (option->r - static_cast<Real>(0.5) * option->sigma * option->sigma) * option->dt;
    Real diffusion = option->sigma * sqrt(option->dt);

    // Initialise the RNG
    curandState localState = rngStates[tid];

    for (unsigned int i = tid ;
         i < numSims ; i += step)
    {
        // Simulate the path
        Real s = static_cast<Real>(1);

        for (unsigned int t = 0 ;
             t < numTimesteps ; t++)
        {
            s *= getPathStep(drift, diffusion, localState);
            paths[i + numSims * t] = s;
        }
    }

}
