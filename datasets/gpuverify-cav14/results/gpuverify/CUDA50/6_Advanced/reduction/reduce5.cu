//pass
//--blockDim=[256] --gridDim=[64]

#include <cuda.h>
#include "common.h"

#define T int
#define blockSize 512





__global__ void kernel (T* g_idata, T* g_odata, unsigned int n) {



    __shared__ T sdata[64 * 256];

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;

    T mySum = (i < n) ? g_idata[i] : 0;

    if (i + blockSize < n)
        mySum += g_idata[i+blockSize];

    sdata[tid] = mySum;
    __syncthreads();

    // do reduction in shared mem
    if (blockSize >= 512)
    {
        if (tid < 256)
        {
            mySum = mySum + sdata[tid + 256];
            sdata[tid] = mySum;
        }

        __syncthreads();
    }

    if (blockSize >= 256)
    {
        if (tid < 128)
        {
            mySum = mySum + sdata[tid + 128];
            sdata[tid] = mySum;
        }

        __syncthreads();
    }

    if (blockSize >= 128)
    {
        if (tid <  64)
        {
            mySum = mySum + sdata[tid +  64];
            sdata[tid] = mySum;
        }

        __syncthreads();
    }

    if (tid < 32)
    {
        // now that we are using warp-synchronous programming (below)
        // we need to declare our shared memory volatile so that the compiler
        // doesn't reorder stores to it and induce incorrect behavior.
        volatile T *smem = sdata;

        if (blockSize >=  64)
        {
            mySum = mySum + smem[tid + 32];
            smem[tid] = mySum;
        }

        if (blockSize >=  32)
        {
            mySum = mySum + smem[tid + 16];
            smem[tid] = mySum;
        }

        if (blockSize >=  16)
        {
            mySum = mySum + smem[tid +  8];
            smem[tid] = mySum;
        }

        if (blockSize >=   8)
        {
            mySum = mySum + smem[tid +  4];
            smem[tid] = mySum;
        }

        if (blockSize >=   4)
        {
            mySum = mySum + smem[tid +  2];
            smem[tid] = mySum;
        }

        if (blockSize >=   2)
        {
            mySum = mySum + smem[tid +  1];
            smem[tid] = mySum;
        }
    }

    // write result for this block to global mem
    if (tid == 0) g_odata[blockIdx.x] = sdata[0];

}
