//pass
//--blockDim=[512,1,1] --gridDim=[32768,1,1]

#include <cuda.h>






__global__ void kernel (int* g_data, int* factor, int num_iterations) {



    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    for (int i=0; i<num_iterations; i++)
    {
        g_data[idx] += *factor;    // non-coalesced on purpose, to burn time
    }

}
