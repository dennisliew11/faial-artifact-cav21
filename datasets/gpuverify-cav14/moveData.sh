#!/usr/bin/env bash

CONTAINER_ID=$(sudo docker ps -qf "name=artifact")
declare -a DataFiles=("faial-stats.pdf" "pug-stats.pdf" "gpuverify-stats.pdf" "stats-legend.pdf" "time-relation-faial-scatter.pdf" "time-relation-legend.pdf" "timings-faial.csv" "timings-gpuverify.csv" "timings-pug.csv" )

#copy each file out of "artifact" container
for f in ${DataFiles[@]}; do
	sudo docker cp $CONTAINER_ID:artifact/datasets/gpuverify-cav14/$f .
done
