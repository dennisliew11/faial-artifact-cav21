//pass
//--blockDim=[128] --gridDim=[128]

#include <cuda.h>






__global__ void kernel (float* g_data, float* uniforms, int n,int blockOffset,int baseIndex) {



    __shared__ float uni[1];
    if (threadIdx.x == 0)
        uni[0] = uniforms[blockIdx.x + blockOffset];
         /* BUGINJECT: MUTATE_OFFSET, UP, ZERO */
    
    unsigned int address = blockIdx.x * (blockDim.x << 1) + baseIndex + threadIdx.x; 

    __syncthreads();
    
    // note two adds per thread
    g_data[address]              += uni[0];
    g_data[address + blockDim.x] += (threadIdx.x + blockDim.x < n) * uni[0];

}
