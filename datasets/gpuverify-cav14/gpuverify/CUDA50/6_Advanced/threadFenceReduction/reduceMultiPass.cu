//pass
//--blockDim=[128] --gridDim=[64]

#include <cuda.h>

#define nIsPow2 true
#define blockSize 128





__global__ void kernel (const float* g_idata, float* g_odata, unsigned int n) {



    __shared__ float sdata[1024];

    // perform first level of reduction,
    // reading from global memory, writing to shared memory
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*(blockSize*2) + threadIdx.x;
    unsigned int gridSize = blockSize*2*gridDim.x;
    float mySum = 0;

    // we reduce multiple elements per thread.  The number is determined by the
    // number of active thread blocks (via gridDim).  More blocks will result
    // in a larger gridSize and therefore fewer elements per thread
    while (i < n)
    {
        mySum += g_idata[i];

        // ensure we don't read out of bounds -- this is optimized away for powerOf2 sized arrays
        if (nIsPow2 || i + blockSize < n)
            mySum += g_idata[i+blockSize];

        i += gridSize;
    }

    // do reduction in shared mem
    sdata[tid] = mySum;
    __syncthreads();

    // do reduction in shared mem
    if (blockSize >= 512)
    {
        if (tid < 256)
        {
            sdata[tid] = mySum = mySum + sdata[tid + 256];
        }

        __syncthreads();
    }

    if (blockSize >= 256)
    {
        if (tid < 128)
        {
            sdata[tid] = mySum = mySum + sdata[tid + 128];
        }

        __syncthreads();
    }

    if (blockSize >= 128)
    {
        if (tid <  64)
        {
            sdata[tid] = mySum = mySum + sdata[tid +  64];
        }

        __syncthreads();
    }

    if (tid < 32)
    {
        if (blockSize >=  64)
        {
            sdata[tid] = mySum = mySum + sdata[tid + 32];
        }

        if (blockSize >=  32)
        {
            sdata[tid] = mySum = mySum + sdata[tid + 16];
        }

        if (blockSize >=  16)
        {
            sdata[tid] = mySum = mySum + sdata[tid +  8];
        }

        if (blockSize >=   8)
        {
            sdata[tid] = mySum = mySum + sdata[tid +  4];
        }

        if (blockSize >=   4)
        {
            sdata[tid] = mySum = mySum + sdata[tid +  2];
        }

        if (blockSize >=   2)
        {
            sdata[tid] = mySum = mySum + sdata[tid +  1];
        }
    }
    // end of

    // write result for this block to global mem
    if (tid == 0) g_odata[blockIdx.x] = sdata[0];

}
