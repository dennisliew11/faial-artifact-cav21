//pass
//--blockDim=[384] --gridDim=[512]

#include <cuda.h>
#include "common.h"

#define min(x,y) (x < y ? x : y)
#define max(x,y) (x < y ? y : x)





__global__ void kernel (Pixel* pSobelOriginal, unsigned int Pitch,int w,int h,float fscale) {

/* kernel pre-conditions */

__requires(w == 512);

__requires(Pitch == 512);





    unsigned char *pSobel =
        (unsigned char *)(((char *) pSobelOriginal)+blockIdx.x*Pitch);

    for (int i = threadIdx.x;
         i < w; i += blockDim.x)
    {
        pSobel[i] = min(max((tex2D(tex, (float) i, (float) blockIdx.x) * fscale), 0.f), 255.f);
    }

}
