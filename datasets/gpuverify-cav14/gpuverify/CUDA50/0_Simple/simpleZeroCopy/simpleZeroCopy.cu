//pass
//--blockDim=[256,1,1] --gridDim=[4096,1,1]

#include <cuda.h>






__global__ void kernel (float* a, float* b, float* c, int N) {



    int idx = blockIdx.x*blockDim.x + threadIdx.x;

    if (idx < N)
    {
        c[idx] = a[idx] + b[idx];
    }

}
