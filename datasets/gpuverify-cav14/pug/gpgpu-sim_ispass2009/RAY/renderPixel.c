#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif
#include "common.h"






__global__ void kernel (uint* result, Node* dnode, uint imageW,uint imageH,float pas,float df) {




__requires(blockDim.x == 32);


__requires(blockDim.y == 32);




__requires(gridDim.x == 1);





	uint id(blockIdx.x + __umul24(blockIdx.y, imageW));
	uint tid(threadIdx.x), x(blockIdx.x), y(blockIdx.y);
	__requires(x < imageW);
    __requires(y < imageH);
    __requires(tid < numObj);

	Node node;
	float t(0.0f), tPixel;
	float4 Color(make_float4(0.0f,0.0f,0.0f,1.0f));
	matrice3x4 M(MView);
	Rayon R;
	Sphere s;
	__shared__ float T[numObj];
	__shared__ uint Obj;

	T[tid] = 10000.0f;
    node = dnode[tid];
    if( tid == 0 ) result[id] = 0;
    tPixel = 2.0f/float(min(imageW,imageH));
    R.A = make_float3(M.m[0].w,M.m[1].w,M.m[2].w);
    R.u = make_float3(M.m[0])*df
        + make_float3(M.m[2])*(float(x)-float(imageW)*0.5f)*tPixel
        + make_float3(M.m[1])*(float(y)-float(imageH)*0.5f)*tPixel;
    R.u = normalize(R.u);
    
    s = node.s;
    s.C.x += pas;

    if( node.fg )
        t = intersectionPlan(R,s.C,s.C);
    else
        t = intersectionSphere(R,s.C,s.r);

    T[tid] = t;

    __syncthreads();

    if( tid == 0 ) {
        float tmp(t);
        Obj = 0;
        for( int i(1); i < numObj; i++ ) {
            if( T[i] > 0.0f && ( tmp == 0.0f || T[i] < tmp ) ) {
                tmp = T[i];
                Obj = i;
            }
        }
    }

    __syncthreads();

    if( tid == Obj && t > 0.0f ) {
        s = node.s;
        s.C.x += pas;
        float3 P(R.A+R.u*t), L(normalize(make_float3(0,1,2)-P)), V(-1*R.u);
        float3 N(node.fg?getNormaleP(P):getNormale(P,s.C));
        if( dot(N,L) > 0.0f ) {
            Color = 0.5f*make_float4(s.R,s.V,s.B,s.A)*(max(0.0f,dot(N,L)));
        #ifdef FIXED_CONST_PARSE
            Color += 0.8f*make_float4(1.0f,1.0f,1.0f,1.0f)*pow(max(0.0f,min(1.0f,dot(2.0f*N*dot(N,L)-L,V))),20.0f);
        #else
        Color += 0.8f*make_float4(1.0f,1.0f,1.0f,1.0f)*float2int_pow20(max(0.0f,min(1.0f,dot(2.0f*N*dot(N,L)-L,V))));
        #endif
        }
        result[id] = rgbaFloatToInt(Color);
    }


}
