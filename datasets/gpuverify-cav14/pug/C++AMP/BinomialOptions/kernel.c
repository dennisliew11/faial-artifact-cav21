#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif

//#define SMALL

#define fast_min(x, y) ((x) < (y) ? (x) : (y))

//////////////////////////////////////////////////////////////////////////////
//// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//// PARTICULAR PURPOSE.
////
//// Copyright (c) Microsoft Corporation. All rights reserved
//////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------
// File: BinomialOptions.cpp
// 
// Implement GPU based binomial option pricing. Verify correctness with CPU 
// implementation
//----------------------------------------------------------------------------

#ifdef SMALL

// Date set - small and normal
// small problem size
#define  MAX_OPTIONS    (32)
#define  NUM_STEPS      (64)
#define  TIME_STEPS     (2)
#define  CACHE_DELTA    (2 * TIME_STEPS)
#define  CACHE_SIZE     (16)
#define  CACHE_STEP     (CACHE_SIZE - CACHE_DELTA)

#else

// normal problem size
#define  MAX_OPTIONS    (512)
#define  NUM_STEPS      (2048)
#define  TIME_STEPS     (16)
#define  CACHE_DELTA    (2 * TIME_STEPS)
#define  CACHE_SIZE     (256)
#define  CACHE_STEP     (CACHE_SIZE - CACHE_DELTA)

#endif

#if NUM_STEPS % CACHE_DELTA
    #error Bad constants
#endif


//----------------------------------------------------------------------------
// GPU implementation - Call value at period t : V(t) = S(t) - X
//----------------------------------------------------------------------------
static __attribute__((always_inline)) __device__ float expiry_call_value(float s, float x, float vdt, int t)
{
    float d = s * exp(vdt * (2.0f * t - NUM_STEPS)) - x;
    return (d > 0) ? d : 0;
}

//----------------------------------------------------------------------------
// GPU implementation of binomial options tree walking to calculate option pricing
// Refer README.txt for more details on algorithm
//----------------------------------------------------------------------------
// Using #ifdef to workaround an exception on Window 7 and Debug build
// Runtime throw's an exception:
//		ID3D11DeviceContext::Dispatch: The Shader Resource View in slot 0 of the Compute 
//  Shader unit is a Structured Buffer while the shader expects a typed Buffer.  This 
// mismatch is invalid if the shader actually uses the view (e.g. it is not skipped due to shader code branching).
// This issue will be fixed in next release.





__global__ void kernel (float* s, float* x, float* vdt, float* pu_by_df, float* pd_by_df, float* call_value, float* call_buffer) {




__requires(blockDim.x == 256);





__requires(gridDim.x == 512);





int tile_idx = blockIdx.x;
int local_idx = threadIdx.x;

__shared__ float call_a[CACHE_SIZE+1];
__shared__ float call_b[CACHE_SIZE+1];

//Global memory frame for current option (thread group)
int tid = local_idx;

// CACHE_SIZE number of thread are operating, hence steping by CACHE_SIZE
// below for loop is similar to first inner loop of binomial_options_cpu
//Compute values at expiry date
for(int index = tid;
    index <= NUM_STEPS; index += CACHE_SIZE)
{
  int idxA = tile_idx * (NUM_STEPS + 16) + (index);
  call_buffer[idxA] = expiry_call_value(s[tile_idx], x[tile_idx], vdt[tile_idx], index);
}

// Walk down binomial tree - equivalent to 2nd inner loop of binomial_options_cpu
//                              Additional boundary checking 
// So double-buffer and synchronize to avoid read-after-write hazards.
for(int i = NUM_STEPS;
  i > 0; i -= CACHE_DELTA)
{

  for(int c_base = 0;
      c_base < i; c_base += CACHE_STEP)
  {
    // Start and end positions within shared memory cache
    int c_start = fast_min(CACHE_SIZE - 1, i - c_base);
    int c_end   = c_start - CACHE_DELTA;

    // Read data(with apron) to shared memory
#ifndef MUTATION
     /* BUGINJECT: REMOVE_BARRIER, DOWN */
    __syncthreads();
#endif
    if(tid <= c_start)
    {
      int idxB = tile_idx * (NUM_STEPS + 16) + (c_base + tid);
      call_a[tid] = call_buffer[idxB];
    }

    // Calculations within shared memory
    for(int k = c_start - 1; 
      k >= c_end;)
    {
      // Compute discounted expected value
      __syncthreads();
      call_b[tid] = pu_by_df[tile_idx] * call_a[tid + 1] + pd_by_df[tile_idx] * call_a[tid];
      k--;

      // Compute discounted expected value
      __syncthreads();
      call_a[tid] = pu_by_df[tile_idx] * call_b[tid + 1] + pd_by_df[tile_idx] * call_b[tid];
      k--;
    }

    // Flush shared memory cache
    __syncthreads();
    if(tid <= c_end)
    {
      int idxC = tile_idx * (NUM_STEPS + 16) + (c_base + tid);
      call_buffer[idxC] = call_a[tid];
    }
  }
}

// Write the value at the top of the tree to destination buffer
if (tid == 0) 
  call_value[tile_idx] = call_a[0];

}
