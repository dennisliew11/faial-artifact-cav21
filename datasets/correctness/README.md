# CAV 2021 RQ1: Correctness Evaluation

## Workflow

1. `./run.py --tool TOOL1 --tool TOOL2 [...]` for each tool to
   generate tool timing CSVs.  While we don't care about benchmarks, this
   creates the logs we want to examine.
2. `./table.py` to display table with results.
3. `./table.py --latex` outputs LaTeX table.
4. Note that the table doesn't reflect if a reported alarm is actually the
   correct data-race.  It's possible that a tool reports a data-race, but that
   the report is wrong.  Because of this, it's important to manually verify the
   correctness of positive reports.

## TODO

- gklee: timeouts looked like array_size pointer errors
  - add support for array_size field per array in TOML kernels in kernel-gen.py
  - set array_size field to kernels sufficent to avoid these errors
- sesa: investigate timeouts. Does fixing these in gklee also fix sesa, or?
