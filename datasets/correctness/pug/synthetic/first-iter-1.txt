----- Data-race 1/1 valid -----
The race is correctly reported.  The following values check out.

t1: 5:wr[tid + 1]
t2: 6:rd[tid]

Both race on x[6].  And as (= !1^y0: 0), the race is reported on the first
iteration, which is correct.


Binary OP: x[(threadIdx.x) +(1)] = 0
containBarrierFlag = 1
incrExpr = y++
------------- Already normalized 
rangeExpr = ((!1^y0 >= 0) && (!1^y0 <= (n0 - 1))); substExpr = 

Binary OP: x[threadIdx.x] = y

FunctionCall: __syncthreads()
satisfiable
(= t1.x 0b0000000111)
(= t2.x 0b0000001000)
(= bdim.x 0b0001000000)
(= n0 0b1000000000)
(= !1^y0 0b0000000000)
(= gdim.x 0b0000000001)
