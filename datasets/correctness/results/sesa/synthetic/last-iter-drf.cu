#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (int* x, int n) {



__requires(n > 0);

for (int y = 0; y < n; y++) {
    __syncthreads();
    if (threadIdx.x < blockDim.x - 1) { // this fixes racyness in last-iter
        x[threadIdx.x + 1] = y;
    }
}
x[threadIdx.x + blockDim.x] = 0;

}
int main () {
    /* Declare scalar 'n' */
    int n;
    klee_make_symbolic(&n, sizeof(int), "n");
    
    /* Declare array 'x' */
    int *x;
    cudaMalloc((void**)&x, 1024 * sizeof(int));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        x,
        n
    );
    return 0;
}
