#include <stdio.h>
#define __requires(x) klee_assume(x)


#define TILE_DIM    16
#define BLOCK_ROWS  16
#define NUM_REPS  100





__global__ void kernel (float* odata, float* idata, int width,int height,int nreps) {



  __requires(height == 2048);
  __requires(width == 2048);
  //__requires(nreps == NUM_REPS);
  __requires(nreps > 0);

  __shared__ float tile[TILE_DIM][TILE_DIM+1];

  int blockIdx_x, blockIdx_y;

  // do diagonal reordering
  if (width == height) {
    blockIdx_y = blockIdx.x;
    blockIdx_x = (blockIdx.x+blockIdx.y)%gridDim.x;
  } else {
    int bid = blockIdx.x + gridDim.x*blockIdx.y;
    blockIdx_y = bid%gridDim.y;
    blockIdx_x = ((bid/gridDim.y)+blockIdx_y)%gridDim.x;
  }    

  // from here on the code is same as previous kernel except blockIdx_x replaces blockIdx.x
  // and similarly for y

  int xIndex = blockIdx_x * TILE_DIM + threadIdx.x;
  int yIndex = blockIdx_y * TILE_DIM + threadIdx.y;  
  int index_in = xIndex + (yIndex)*width;

  xIndex = blockIdx_y * TILE_DIM + threadIdx.x;
  yIndex = blockIdx_x * TILE_DIM + threadIdx.y;
  int index_out = xIndex + (yIndex)*height;

  for (int r=0; r < nreps; r++) {
    for (int i=0; i<TILE_DIM; i+=BLOCK_ROWS) {
      tile[threadIdx.y+i][threadIdx.x] = idata[index_in+i*width];
    }
  
    __syncthreads();
  
    for (int i=0; i<TILE_DIM; i+=BLOCK_ROWS) {
      odata[index_out+i*height] = tile[threadIdx.x][threadIdx.y+i];
    }

    __syncthreads(); // sync added to fix race
  }

}
int main () {
    /* Declare scalar 'width' */
    int width;
    klee_make_symbolic(&width, sizeof(int), "width");
    /* Declare scalar 'height' */
    int height;
    klee_make_symbolic(&height, sizeof(int), "height");
    /* Declare scalar 'nreps' */
    int nreps;
    klee_make_symbolic(&nreps, sizeof(int), "nreps");
    
    /* Declare array 'odata' */
    float *odata;
    cudaMalloc((void**)&odata, 4194304 * sizeof(float));
    
    /* Declare array 'idata' */
    float *idata;
    cudaMalloc((void**)&idata, 4194304 * sizeof(float));
    dim3 grid_dim(64, 64);
    dim3 block_dim(16, 16);
    kernel<<< grid_dim, block_dim >>>(
        odata,
        idata,
        width,
        height,
        nreps
    );
    return 0;
}
