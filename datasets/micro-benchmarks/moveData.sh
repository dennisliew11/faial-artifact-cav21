#!/usr/bin/env bash

CONTAINER_ID=$(sudo docker ps -qf "name=faial-cav21")
declare -a DataFiles=("timings-faial-accs.csv" "timings-faial-barriers.csv" \
"timings-faial-ifs.csv" "timings-faial-nested-loops.csv" \
"timings-faial-nested-loops-sync.csv" \
"timings-gpuverify-accs.csv" "timings-gpuverify-barriers.csv" \
"timings-gpuverify-ifs.csv" "timings-gpuverify-nested-loops.csv" \
"timings-gpuverify-nested-loops-sync.csv" \
"timings-pug-accs.csv" "timings-pug-barriers.csv" \
"timings-pug-ifs.csv" "timings-pug-nested-loops.csv" \
"timings-pug-nested-loops-sync.csv" \
"timings-gklee-accs.csv" "timings-gklee-barriers.csv" \
"timings-gklee-ifs.csv" "timings-gklee-nested-loops.csv" \
"timings-gklee-nested-loops-sync.csv" \
"timings-sesa-accs.csv" "timings-sesa-barriers.csv" \
"timings-sesa-ifs.csv" "timings-sesa-nested-loops.csv" \
"timings-sesa-nested-loops-sync.csv" \
"Micro-benchmark-memory-1-50.pdf" "Micro-benchmark-time-1-50.pdf" \
"microbenchmark-legend.pdf" )

#copy each file out of "artifact" container
for f in ${DataFiles[@]}; do
	sudo docker cp $CONTAINER_ID:artifact/datasets/micro-benchmarks/$f .
done
