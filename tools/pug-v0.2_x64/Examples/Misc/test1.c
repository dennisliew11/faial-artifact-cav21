#include "my_cutil.h"

extern __shared__ int s[];

void __global__ knel0 (int *out) {
  int id = threadIdx.x;
  s[id] = s [id * id];
}

void __global__ kernel (int *out) {
  int id = threadIdx.x;
  int j;

/*
  s[id+2] = 0;
  if (id > 5) {
    bar();
    s[id] = 0;
  }
  else {
    bar();
    s[id+1] = 0;
  }
*/
  if (id < 5) {
    if (blockIdx.x > 10) {
      /*     __syncthreads(); */
      /*     j = s[id]; */
      /*   } */
      /*   else { */
      /*     int k = 0; */
      s[id+1] = 15;
      __syncthreads();
    }
    else {
      bar();
    }
  }

//  s[id] = 0;

}
