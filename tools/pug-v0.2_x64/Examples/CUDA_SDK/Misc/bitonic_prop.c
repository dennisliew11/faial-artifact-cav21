
#ifndef _BITONIC_KERNEL_CU_
#define _BITONIC_KERNEL_CU_

#include "my_cutil.h"

#define NUM 2

extern __shared__ int shared[];

__global__ void kernel(int values[])  // bitonic sort
{
  int tmp;

  unsigned int tid = threadIdx.x;
  
  // Copy input to shared mem.
  shared[tid] = values[tid];
  
  __syncthreads();
  
  unsigned int ixj;

  // Parallel bitonic sort.
  // for (unsigned int k = 2; k <= NUM; k = k + k)
  {
    // Bitonic merge:
    // for (unsigned int j = k >> 1; j > 0; j = j >> 1)
    {
      ixj = tid ^ 1;
      
      // Bitonic split
      if (ixj > tid)
	{
	  if ((tid & 2) == 0)
	    {
	      if (shared[tid] > shared[ixj])
		{
		  tmp = shared[tid];
		  shared[tid] = shared[ixj];
		  shared[ixj] = tmp;
		}
	    }
	  
	  else
	    {
	      if (shared[tid] < shared[ixj])
		{
		  tmp = shared[tid];
		  shared[tid] = shared[ixj];
		  shared[ixj] = tmp;
		}
	    }
	}
      
      __syncthreads();
    }
  
  // Write result.
  // values[tid] = shared[tid];
  }

}


void guarantee () {
  assert(shared[0] <= shared[1]);
}


#endif // _BITONIC_KERNEL_H_
