# Kernel Generation Framework

This document provides more detail on the use of our kernel generation and
benchmarking framework.

## Motivation

A  factor that hampers comparative studies of tools is that most tools require
alterations of source programs before analysis.  And as each alteration is
specific to each tool, required dataset preparation can be substantial.  It is
therefore challenging to conduct a large-scale, tool-diverse study that targets
real-world GPU kernels.

We address this challenge by storing kernels in a tool-agnostic format and
using templates to generate tool-specific programs for evaluation.  Python
scripts are provided to automate this generation process and to run experiments
against large datasets of GPU kernels.


## Configuration

Editing the configuration file `config.yaml` is the best way to alter
experimental parameters (if needed) as it is read by scripts in the beginning
of the pipeline.

**NOTE: `config.yaml` is used by both `run.py` and `benchmark-graph.py`;
however, each claim has a unique configuration file in its respective
directory.**

`config.yaml` parameters:

| Claim | Name         | Description                                           |
|-------|--------------|-------------------------------------------------------|
| 2     | block_dim    | block dimension                                       |
| 2     | grid_dim     | grid dimension                                        |
| 2     | problems     | specifies the synthetic protocols                     |
| all   | tools        | configures tools and their command line call          |
| 2     | formats      | specifies kernel generation formatting (this should not need any editting) |
| 2     | default_range| configures size of protocol to generate and run       | 
| all   | timeout      | configures timeout for experiment                     | 
| 2     | range        | specifies timeout cutoff range for experiment for each Tool and protocol (these values were determined based on our machine capabilities. Faster machines should increase the cutoff number.) | 
| 1, 3  | kernels      | specifies the kernels to run                          |

## Generating Claim Dataset

Our dataset generation framework (located in the `benchmark/contrib` directory)
allows for the use of tool-agnostic GPU kernels stored in TOML files: this
format is used for [Claim 1] and [Claim 3].  The synthetic protocol patterns
used in [Claim 2] are located in `benchmark/contrib/tpl`, and the tool-specific
dataset kernels are generated from these patterns.

Generation of tool-specific kernels for [Claim 1] and [Claim 3] is done
automatically by `run.py`.  To generate the tool-specific kernels of [Claim 2]:

```shell
$ cd $FAIAL_HOME/datasets/micro-benchmarks
$ python3 generate.py
```

`generate.py` uses `config.yaml` from the same directory. The [configuration
parameters](#configuration) can be edited to determine which kernels are
generated. 

[Claim 1]: README.md#31-claim-1-correctness
[Claim 2]: README.md#32-claim-2-scalability
[Claim 3]: README.md#33-claim-3-real-world-usability
